﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

# The game starts here.

label intro01:
    scene ggjsplash with fade

    pause 2
    
    scene black with fade    
    play music bgmtensionbed fadeout 3.0
    play ambient ambfireplace fadein 1.0
    
    "After nearly a day of running, there's finally signs of civilisation ahead."

    scene campnight with Dissolve(3.0)
    show pro neutral at left with moveinleft

    $ renpy.sound.set_volume(0.5)
    play sound ambcomputer3
    "It's a small camp, with a radio tower."
    "There's a figure on watch, but you can't make out who, or what, they are from this distance."
    pro "This is the only way through..."
    menu:
        "Stealthily approach the camp.":
            "You slowly advance towards the camp."
            "The human on watch glances in your direction. It's possible they've spotted you."
        "Sneak around the edge.":
            "You slowly advance around to the side of the camp."
            "The human on watch glances in your direction. It's possible they've spotted you."
        "Approach the camp with your hands raised.":
            "You raise your hands in submission and walk towards the camp."
            "The human on watch glances your way before raising their rifle."
    
    show pro code red with dissolve
    pro "\[Combat::Threat] THREAT DETECTED. ENGAGING COMBAT PROTOCOLS."
    pro "\[Combat::Threat] FOUR HUMANS DETECTED. ANALYSIS: ELIMINATE."
    menu:
        "Kill the humans.":
            pass
        "Kill the humans.":
            pass
        "Kill the humans.":
            pass
    play music bgmactionbed fadeout 1.0
    pro "\[Combat::Engage] OPTION CONFIRMED. COMBAT PROTOCOLS ENTERING 'MURDERBOT' STATE"
    
    show mec at right with moveinright
    mec "MACHINE!"
    play sound sfxgunshot
    "*BANG!!*"

    pro "\[Combat::Engage] FIREARM THREAT DETECTED. EVADING."
    pro "\[Combat::Tactical] HUMAN RADIO RISKS REINFORCEMENTS. DESTROY."

    menu:
        "Destroy the radio.":
            pass
        "Destroy the radio.":
            pass
        "Destroy the radio.":
            pass

    $ renpy.sound.set_volume(1.0)
    play sound sfxsmash
    "As your fist disappears into the radio, sparks explode out into every direction."

    mec "That's enough, toaster!"

    stop music fadeout 0.5
    play sound sfxheadshot
    show pro with dissolve
    "*BANG!!*"

    #another gunshot, hits the head
    #robo shutting down noise
    #fade/snap to black?
    scene black with Dissolve(0.7)
    "The human's shot connects with your head, and you fall to the ground."

    jump camp02
