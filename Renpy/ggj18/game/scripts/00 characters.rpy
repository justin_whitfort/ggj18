﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

#PROTAG
define pro = DynamicCharacter("pro_name")

# HUMANS
define mec = Character("Mechanic")

define com = Character("Leader")

define doc = Character("Doctor")

define kid = Character("Kid")