﻿# The script of the game goes in this file.

# The game starts here.

label morning07:
    scene campmorning
    play ambient ambgun2 fadein 2.0
    $ renpy.music.set_volume(0.2)
    play music bgmdefaultintro noloop fadeout 1.0
    queue music bgmdefaultloop loop

    show pro neutral at left

    show com with dissolve:
        align (0.4, 1.0)
    show mec with moveinright:
        align (0.6, 1.0)
    show kid with dissolve:
        align (0.8, 1.0)
    show doc with dissolve:
        align (1.0, 1.0)

    com "So."
    com "You've broken our radio! We're miles from the next observation post!"
    com "And who knows how close behind you your friends are!?"

    kid "It probably knows, ma'am!"

    com "Silence! And get back to cooking!"

    kid "Yes, ma'am! Sorry, ma'am."

    com "So, Toaster! How close are your friends?"

    menu:
        "I ran ahead.":
            pro "I ran ahead. The main force is eight days away."
            com "Obviously! They've been moving at the same speed all year!"
            com "How close are the scouts? They could be anywhere!"
        "The scouts could be anywhere.":
            pro "The scouts could be anywhere! I wasn't supposed to be scouting. I'm not sure where they're up to!"
            com "A likely story!"

    doc "I have some... suggestions. Ways of making it... talk."

    com "A good thought, doc, but we're miles from the nearest pub! And I'm not sure Toasters drink!"

    menu:
        "I haven't had a drink in...":
            pro "I haven't had a drink in... at least a decade."
            doc "Oh, come on, commander! This is serious!"
        "Drinking isn't great for my circuits.":
            pro "Drinking isn't great for my circuits."
            com "That's practically the point, Toaster! Isn't good for any of our circuits!"

    kid "Potatoes are ready!"

    doc "Oh, boy. What is it this time, boiled?"

    kid "Yes, sir!"

    doc "Fantastic."

    menu:
        "Save some for me.":
            mec "Sure! How do you like your potato? Chips?"
            doc "Please stop encouraging it."
        "You're having potato? For breakfast?":
            mec "Well, yeah. Your lot took half the crops in the country."
            mec "What did you think we ate?"
    
    com "Mechanic! How long until our radio is fixed?"
    
    mec "I'm eating."
    
    com "MECHANIC!"
    
    mec "To be honest, commander, it's stuffed. I'd need at least a few replacement parts."
    
    com "We don't have replacement parts! And command need to know about this Toaster immediately!"
    
    kid "Can't we take the car?"
    
    com "Someone will have to walk to the next outpost and radio command!"
    
    kid "...could that person drive?"
    
    doc "Can you drive, kid?"
    
    kid "Well, no, but someone here..."
    
    com "Ever tried driving stick with one hand, Rookie?"
    
    kid "No, I haven't-"
    
    mec "I can't drive either."
    
    menu:
        "You have a car, but none of you can drive?":
            pro "You have a car, but none of you can drive?"
            com "Silence, Toaster!"
        "I think I can remember how to drive.":
            pro "I think I can remember how to drive..."
            mec "Remember? You forget things like that?"
            com "At any rate, no one's getting killed by the Toaster's driving!"
    
    com "Someone will have to walk!"
    
    doc "We should send the kid. He's probably the fastest."
    
    kid "You just don't want to go yourself!"
    
    mec "I can probably sneak through."
    
    com "Whilst I'm honored you think I'm up to it, I am too crippled by that damned Kraut!"
    
    com "Doctor, you are the most likely to patch yourself up to survive the inevitable near mortal wounds!"
    
    doc "What? The other two just volunteered!"
    
    $ includeKid = True
    $ includeDoc = True
    $ includeMec = True
    
    menu:
        "The Kid didn't volunteer.":
            pro "The Kid didn't volunteer."
            com "You're unfortunately right, Toaster. The Recruit's just not cut from the same stuff as me."
            $ includeKid = False
        "Shouldn't the mechanic be fixing the radio?":
            pro "Shouldn't the mechanic be fixing the radio?"
            com "Good point, toaster! She's far too important to our position here."
            $ includeMec = False
        "The doctor seems too cowardly to make it.":
            pro "The doctor seems too cowardly to make it."
            com "Good point, Toaster! He's deserted his post once before already!"
            $ includeDoc = False
    
    mec "That still leaves us with two people."
    
    menu:
        "The Kid is fast." if includeKid:
            pro "The Kid is young. He might be faster than some of the robots."
            kid "Oh, I guess. What about the other robots?"
            $ includeMec = False
            $ includeDoc = False
        "The Doctor can patch himself up." if includeDoc:
            pro "None of you will be able to outrun any scouts out there. The doc might be able to patch himself up afterwards."
            doc "Are you serious? Don't tell me any of you are planning on listening to this thing."
            $ includeMec = False
            $ includeKid = False
        "The mechanic has experience." if includeMec:
            pro "I suspect the mechanic is the only one of you who's ever shot a robot."
            mec "Uhh, yeah. I guess I have."
            $ includeKid = False
            $ includeDoc = False
    
    com "It's settled then! And there's no time to leave like the present!"
    com "Hop to it, soldier!"
    
    if includeMec:
        mec "Sure. Just let me finish my potato."
        "WILL THE MECHANIC MAKE IT TO THE NEXT CAMP ALIVE?"
    if includeKid:
        kid "Oh! Uh. Yes ma'am!"
        kid "...can I finish my breakfast first?"
        "WILL THE KID MAKE IT TO THE NEXT CAMP ALIVE?"
    if includeDoc:
        doc "This is a terrible idea."
        doc "I hope you all break your legs while I'm gone."
        "WILL DOC MAKE IT TO THE NEXT CAMP ALIVE?"
    
    "WILL A-0027 REMEMBER MORE OF THEIR PAST?"
    "WILL THE WAR EVER END?"
    
    "FIND OUT NEXT TIME ON {i}OP DODGE CROSS{/i}"
    
    pause 1
    
    scene black with fade
    
    pause 2