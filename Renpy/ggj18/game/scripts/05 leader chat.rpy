﻿# The script of the game goes in this file.

label leader05:
    $ visited_leader_05 = True
    scene campnight
    show pro neutral at left
    show com:
        align (1.3, 1.0)
    show com at right:
        linear 1.0 align (1.0, 1.0)
    pause 1.0

    "The leader stares at you intently."
    show com stern
    com "So, foul robot!"
    show com
    
    menu:
        "So, foul Human.":
            pro "So, foul Human."
        "...":
            pro "..."

    show com stern
    com "Aha! So that's how it's going to be!"

    com "I've seen your type before!"
    com "Maybe not in person, but I've read the field manual!"
    show com
    
    menu:
        "Wouldn't that mean you haven't seen 'my type' before?":
            pro "Wouldn't that mean you haven't seen 'my type' before?"
        "I get the feeling you don't have much field experience.":
            pro "I get the feeling you don't have much field experience."
            show com stern
            com "Field experience? I'm the most experienced one here!"
            com "I was fighting the Krauts long before you damned robots were here!"

    com "But you aren't even like the rest of them, are you!"
    com "Who ever heard of a robot surrendering!?"
    com "There's something odd about you, and we're going to find out what!"
    com "And then I'm going to be promoted back to my rightful rank!"
    show com
    
    menu:
        "Rightful rank?":
            pro "Rightful rank? Were you higher ranked in the past?"
            com "Quiet, robot! I was robbed of my rightful future ranks when I was robbed of my arm!"
        "Something odd about me?":
            pro "Something... odd about me?"
            com "That's damn right, robot! You're an odd one!"
            com "And if I bring you in, they'll have to overlook my little war wound and give me the rank I deserve!"
    show com stern
    com "Damn you, Kraut! I'll get you next time!"
    show com
    com "Well, to be fair, someone else got him that time, but that's not the point!"

    com "You should have seen me in the old days! Back before this damned war with your lot!"
    com "I had more medals than you'd believe!"
    com "Those Krauts didn't know what hit them!"
    show com stern
    com "The Fiend of Bexhill, they called me!"

    com "No one could stop me! Not until the war ended and one damn Kraut hadn't got the message."
    show com
    
    menu:
        "How did the war end?":
            pro "How did the war end?"
            show com stern
            com "How did the war end!?"
            show com
            com "I guess I shouldn't expect a damn robot to know its history!"
        "Hadn't got the message?":
            pro "What do you mean, hadn't got the message?"
            show com
            com "He shot me straight through the elbow! Right after their government had surrendered!"
            com "He only got me because we'd been told to stop shooting the bastards!"
    com "It was the winter of '45!"
    show com
    com "We were having a jolly good fight on the coasts of England, when the damn Yanks finally show up!"
    com "They drop a few of those Atomic Bombs, and the Krauts surrender overnight!"
    
    show com stern
    com "And then your mob go and show up six days later! Six days!"
    show com
    
    menu:
         "I didn't realise it was so short.":
             pro "I didn't realise it was so little time."
         "I remember the invasion...":
            pass

    pro "I remember the German invasion, then when we started ours."
    pro "Nothing in between."

    show com stern
    com "You remember the invasion!?"
    show com
    
    pro "We were so desperate..."

    show com stern
    com "The cowards were! The real soldiers had the Krauts right where we wanted them!"
    show com
    
    menu:
        "Real soldiers? You wanted the killing to go on?":
            pro "Real soldiers? You wanted the killing to go on?"
        "I'm not sure if I should think you're brave or stupid. Or just disgusting.":
            pro "I'm not sure if I should think you're brave or stupid. Or just disgusting."
            show com stern
            com "Disgusting?!"
            show com
    show com stern
    com "..."
    show com
    com "..."
    com "I'm actually lost for words."
    com "This hasn't happened to me in a decade."
    com "Aren't you a killer robot, with the killer robots doing a pretty good job of killing the entirety of humanity?"

    pro "...I see your point."

    "She stayed silent for the rest of her shift."

    show com stern
    com "It's been fun, killer robot, but my shift is done."
    show com
    
    if visited_doctor_04:
        com "Let's see if the mechanic can keep her hands off you..."
        hide com with dissolve
        "The commander disappears into one of the tents, and moments later the mechanic trots out."
        jump mechanic06
    else:
        com "Have fun with the doctor, killer robot."
        com "And good luck..."
        hide com with dissolve
        "The commander disappears into one of the tents, and about a minute later the doctor steps out with a scowl."
        jump doctor04
