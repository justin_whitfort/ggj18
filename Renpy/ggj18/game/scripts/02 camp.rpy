﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

# The game starts here.

label camp02:

    scene black
    play sound sfxbootup
    
    "REBOOTING. SYSTEM INTEGRITY CHECK INITIATED."
    "....."
    "CORE PROCESSING TAKEN DAMAGE IN REDUNDANT SYSTEMS. REROUTING."
    "LOADING CORE MODULES."
    "CORE MODULES LOADED. LOADING BEHAVIOUR MODULES."
    "BEHAVIOUR MODULES LOADED. LOADING ADDITIONAL MODULES."
    "....."
    "MODULE TACTICAL_DIRECTIVES (AND 13 OTHERS) FAILED TO INITIALISE DUE TO CONFLICTS WITH RECENTLY INSTALLED MODULES."

    "ALL CRITICAL MODULES LOADED. LAUNCHING CORE_BEHAVIOUR_13.2_ALPHA_EXPERIMENTAL..."

    $ renpy.music.set_volume(0.8)
    play music bgmdefaultintro noloop fadeout 1.0
    queue music bgmdefaultloop loop
    play ambient ambfireplace fadeout 1 fadein 0.5
    scene campnight with Dissolve(3.0)

    show pro at left with dissolve

    #TODO: delay commander's appearance by a bit
    show com stern with dissolve

    com "Good shot, Mechanic!"

    show mec at right with moveinright

    mec "Shh, I don't think it's dead."

    show com with dissolve
    com "\"Dead\"? I didn't realise you saw The Machines as alive. I've learned something new about you today."

    show pro neutral with dissolve

    mec "Commander, please be quiet... dammit, it's moving!"

    com "Go on, Mechanic. Shoot it again. Or are you morally against \"killing\" machines?"

    show mec blush with dissolve
    mec "I'm *not*... nevermind, I'll just shoot it--"

    pro "I surrender."

    show com stern with dissolve
    com "Did it say something?"

    show mec with dissolve
    mec "Commander, please get out of the way."

    com "Sure. Make sure you get it in the middle of its head this time."

    pro "I surrender!"

    mec "Did it just say..."

    pro "This unit surrenders."
    show pro logo with dissolve

    show com with dissolve
    com "Oh, that's new."

    mec "Uh, what are we supposed to do when one of them surrenders?"

    com "Beats me. I've never seen this happen before. Let's just shoot it anyway."

    show mec:
        linear 0.3 align (0.7, 1.0)
    show kid surprise at right with moveinright:
        align (0.9, 1.0)

    kid "Wait, what about the, you know, Geneva Convention thingy? The one about providing proper treatment to prisoners of war?"

    com "Proper treatment for prisoners of war? What idiot would come up with such hogwash?"
    com "Oh, wait, I think I remember something like that."
    
    show com stern with dissolve
    com "Hang on, where did you come from, Rookie?"

    show kid with dissolve
    kid "I woke up. I'm not as good at sleeping through gunshots as everyone else."

    mec "Uh, commander? Your orders?"

    show com with dissolve
    com "Let's ask headquarters."

    mec "About that..."

    show com:
        linear 0.3 align (0.4, 1.0)
    show mec:
        linear 0.3 align (0.6, 1.0)
    show kid:
        linear 0.3 align (0.8, 1.0)
    show doc with moveinright:
        align (1.0, 1.0)

    doc "*Yawn*"
    doc "What's going on? Why's our transmission radio broken?"
    doc "Wait, is that a..."
    
    show doc glare with dissolve
    doc "...I knew I shouldn't have woken up."

    show com stern with dissolve
    com "You sabotaged our transmission radio?!"

    pro "..."

    show com with dissolve
    com "That's an excellent tactical move. Good job."

    doc "What are you praising the toaster for? Just shoot it already."

    show kid surprise with dissolve
    kid "Well, there's the Geneva Convention thingy..."

    doc "Fuck that. Do you want it to kill us in our sleep?"

    mec "We can tie it up. It won't be able to move."

    doc "There's no reason to keep it alive! The Geneva Convention is for {i}living{/i} things!"

    jump newbie03
