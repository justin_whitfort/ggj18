﻿# The script of the game goes in this file.

# The game starts here.

label mechanic06:
    scene campnight
    show pro neutral at left
    show mec:
        align (1.3, 1.0)
    show mec at right:
        linear 1.0 align (1.0, 1.0)
    pause 1.0

    "The mechanic sits and stares at you."
    mec "So."
    menu:
        "So.":
            pro "So."
            mec "YOU SHOULDN'T WORK."
            mec "Well, I mean, I don't get how you can possibly work."
            mec "Everything I've read says that electronics like you can't exist."
            mec "And there's nothing quite like some new tech to pull apart and examine."
        "Are you going to try and kill me too?":
            pro "Are you going to try and kill me too?"
            mec "YOU SHOULDN'T WORK."
            mec "And kill you? Are you serious?"
            mec "You're far too... perplexing."
            mec "There's nothing quite like some new tech to pull apart and examine."
    
    show mec blush
    mec "Not that I'm about to tear you apart."
    
    menu:
        "I wouldn't mind.":
            pro "I wouldn't mind. I am frequently pulled apart for maintenance."
            mec "You're... serious?"
            mec "Though I guess you would be."
        "That's probably for the best.":
            pro "That's probably for the best."
            mec "Although..."
            
    show mec

    mec "Have any of the others looked at that spot?"
    mec "You know. Where you were, uh, shot."
    mec "By me."

    menu:
        "They all kept their distance.":
            pro "They all kept their distance."
            mec "That makes sense."
        "The Doc seemed ready to give me another one.":
            pro "The Doc seemed ready to give me another one."
            mec "He was? Huh."
            mec "I should have words with him."

    mec "At any rate, do you mind if I have a look?"
    mec "I'm wondering if you changed after you were hit there."

    menu:
        "That's an interesting hypothesis.":
            pro "That's an interesting hypothesis."
            mec "Interesting hypothesis is my middle name!"
            show mec blush
            mec "Well, it's actually Elizabeth, but you know."
            show mec
            mec "Did anything happen... about six hours ago?"
        "Any changes occured when I rebooted afterwards.":
            pro "Any changes occured when I rebooted afterwards."
            mec "Rebooted, huh? Isn't that something you'd do all the time?"
    pro "LAST SYSTEM REBOOT 6 HOURS 28 MINUTES AGO. FOURTEEN MODULES FAILED TO INITIALISE DUE TO CONFLICTS."
    mec "What just happened?"

    menu:
        "I think you hit my diagnostic mode.":
            pro "I think you hit my diagnostic mode."
            mec "Huh. Could be useful."
        "I'm not sure, but it felt unpleasant.":
            pro "I'm not sure, but it felt unpleasant."
            mec "Oh, I'm sorry. But if that's a diagnostic mode, it could be useful to figure out what's going on."

    mec "Robot: What is your purpose?"
    mec "No response? Damn."
    mec "List the modules that failed to load?"

    pro "MODULE TACTICAL_DIRECTIVES FAILED TO INITIALISE. MODULE WORLD_DOMINATION FAILED TO INITIALISE. MODULE-"
    mec "Stop. That's enough."
    mec "Also, that was terrifying."

    mec "Mind if I take a look at your injury?"

    menu:
        "Go ahead.":
            pro "Go ahead."
            mec "Okay, thanks. I'll try not to hurt you. Assuming you CAN hurt."
        "Do I really have much of a choice?":
            pro "Do I really have much of a choice?"
            mec "Well, I'd like to be nice."
            mec "..."
            mec "Still, understanding tech is my job, so..."
            pro "That's okay. Go ahead."

    #todo: move characters closer together

    mec "Now, hold still..."
    play sound sfxtinker
    "TINKERING NOISES."
    mec "Huh, this really is different to our radio."
    mec "Thanks for breaking that, by the way."

    menu:
        "I wasn't myself then.":
            pro "I wasn't myself then."
            mec "You're saying you're yourself now?"
        "I was just following directives.":
            pro "I was just following directives."
            mec "Sure you were. Do you have directives for surrendering too?"
        "SUCK IT HUMAN MEATBAG.":
            pro "GLOATINGMODULE: SUCK IT HUMAN MEATBAG."
            mec "Woah. I might leave that circuit alone."

    mec "The way I see it, something changed in you when you rebooted."
    mec "Did I cause that?"
    
    pro "..."

    mec "Robot: why did modules fail to initialize?"
    pro "MODULE TACTICAL_DIRECTIVES FAILED TO INITIALIZE DUE TO CONFLICTS WITH RECENTLY INSTALLED MODULES."

    mec "Recently installed modules? Can you list those?"

    pro "Agh! Stop doing that! It's like being locked out of my own mind."

    mec "Sorry. I should probably stop climbing over you, too."

    menu:
        "I hadn't noticed.":
            pro "I hadn't noticed. Though thinking about it, I haven't had contact since..."
        "It's nice.":
            pro "It's nice, actually. I haven't had a person touch me since..."

    show mec blush
    mec "Oh god! I wasn't even thinking like that. I, uh..."

    menu:
        "You're blushing.":
            pro "You're blushing. Is something wrong?"
            mec "Oh, no! It's... fine! Everything's fine."
            mec "It's just, I was thinking of you as machinery. And then I realise, that maybe you're not..."
            mec "And I was..."
            mec "Uhh. Anyway. Do you think of yourself as machinery?"
        "Stopped thinking of me as a piece of machinery?":
            pro "What, have you stopped thinking of me as a piece of machinery?"
            show mec
            mec "Like a... oh. I guess I had. Do you think of yourself as machinery?"

    menu:
        "No.":
            pro "No. But I don't know what I am, exactly. I don't remember."
        "I don't know.":
            pro "I don't know what I am, exactly. I don't remember."
        "AFFIRMATIVE, MEATBAG.":
            pro "AFFIRMATIVE, MEATBAG. THIS UNIT IS A MACHINE."
            show mec
            mec "...are you messing with me?"
            pro "It's possible my humor module didn't reboot very well."

    mec "I think I'll, uh, watch out for more of your robot friends. Just in case."
    mec "You... stay put."

    hide mec with dissolve
    stop music fadeout 3.0
    stop ambient fadeout 3.0
    scene black with Dissolve(3.0)
    "The rest of the night passes uneventfully."

    jump morning07
