﻿# The script of the game goes in this file.

label doctor04:
    $ visited_doctor_04 = True

    scene campnight
    show pro neutral at left

    show doc:
        align (1.3, 1.0)
    show doc at right:
        linear 1.5 align (1.0, 1.0)
    pause 1.5

    doc "..."
    
    $ renpy.music.set_volume(0.4, delay=5.0)
    $ renpy.music.set_volume(0.6, channel="ambient")
    play ambient bgmtensionbed fadeout 1.0 fadein 4.0

    "The doctor isn't saying anything. He's just staring scalpels at you."

    doc "..."

    "He's still saying nothing."

    menu:
        "Don't you need to keep watch?":
            pro "Don't you need to watch out for threats?"
            doc "I am."
            pro "You mean me, don't you."
            doc "So you're admitting you're a threat."
        "Why are you looking at me like that?":
            pro "Why are you looking at me like that?"
            doc "Just waiting for a reason."
            pro "A reason to what?"
            doc "A reason for self defense."

    show doc glare with dissolve
    "The doctor darkens his stare at you."
    pro "I'm no threat when I'm tied up like this."
    doc "Yes, you're tied up. Which begs the question - why haven't I hacked you into pieces yet?"

    menu:
        "That's not very doctor-like.":
            pro "That's not very doctor-like."
            doc "What does hacking apart a toaster got to do with being a doctor?"
            doc "Besides, I've never wanted to be a doctor, so I was never \"doctor-like\"."
        "Why haven't you?":
            pro "So why haven't you?"
            doc "Because that idiot commander of ours said it was an order."
            doc "I'm sick of being a doctor. Especially to these hopeless cases."

    pro "Why choose to be a doctor, then?"
    doc "Oh, did you think I had a choice? When you waged your war against us, did you think no one would be hurt?"
    pro "You were conscripted."
    doc "Yes."

    "The doctor continues to scowl at you, his eyes never wavering."
    "You get the impression the doctor is silently weighing the pros and cons between going against orders and killing you, versus waiting for a better opportunity."

    menu:
        "Keep talking.":
            pro "Perhaps we've started on the wrong foot--"
            doc "Quiet. After being woken up in the middle of my sleep by an intruder, I'm not feeling particularly patient."
        "Wait until next shift.":
            pass

    "Deciding that your survival is highest if you say nothing else, you wait silently as time passes."

    show doc with dissolve
    doc "It's time for the next shift."
    play ambient ambfireplace fadeout 3.0 fadein 1.0
    $ renpy.music.set_volume(1.0, delay=3.0, channel="ambient")
    $ renpy.music.set_volume(0.8, delay=5.0)
    if visited_leader_05:
        doc "Maybe when I next see you, the mechanic would have separated you into a dozen parts. That's something to look forward to."
        hide doc with dissolve
        "The doctor disappears into one of the tents, and moments later the mechanic trots out."
        jump mechanic06
    else:
        doc "Hopefully you'll make a wrong move and the commander shoots you. Isn't that a lovely thought."
        hide doc with dissolve
        "The doctor disappears into one of the tents, and about a minute later the commander stumbles out groggily."
        jump leader05
