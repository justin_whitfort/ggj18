﻿# The script of the game goes in this file.

# The game starts here.

#######
# BGM #
#######
define bgmdefaultintro = "Sound/Music/OP Dodge Cross swing (old radio) with intro.mp3",
define bgmdefaultloop = "Sound/Music/OP Dodge Cross swing (old radio) loopable.mp3",
define bgmreverb = "Sound/Music/OP Dodge Cross swing with reverb (for mood beds).mp3",
define bgmactionbed = "Sound/Music/OP Dodge Cross action mood bed.mp3",
define bgmtensionbed = "Sound/Music/OP Dodge Cross tension mood bed.mp3",

#######
# AMB #
#######
define ambfireplace = "Sound/Ambience/fireplace.wav",
define ambfireplace2 = "Sound/Ambience/fireplace w ambience.wav",
define ambcomputer1 = "Sound/Ambience/computer sound.wav",
define ambcomputer2 = "Sound/Ambience/computer sound 2.wav",
define ambcomputer3 = "Sound/Ambience/computer sound 3.wav",
define ambcomputer4 = "Sound/Ambience/computer running.wav",
define ambgun = "Sound/Ambience/ambient distant gun shots.wav",
define ambgun2 = "Sound/Ambience/ambient distant gun reverb.wav",
define ambgundrone = "Sound/Ambience/ambience distant gunshots w drone.wav",


#######
# SFX #
#######
define sfxgunshot = "Sound/SFX/close gunshot.wav",
define sfxheadshot = "Sound/SFX/gun shot on helmet w robot shutdown.wav",
define sfxbootup = "Sound/SFX/computer boot up.wav",
define sfxtinker = "Sound/SFX/head tinkering.wav",
define sfxsmash = "Sound/SFX/radio being smashed.wav",

init python:
    renpy.music.register_channel("ambient","sfx",loop=True,tight=True)

label start:
    
    $ pro_name = "A-0027"
    $ visited_doctor_04 = False
    $ visited_leader_05 = False

    #play music bgmdefaultintro noloop fadeout 1.0
    #queue music bgmdefaultloop loop

    menu:        
        "01 Intro":
            jump intro01
        "02 Camp":
            jump camp02
        "03 Newbie Chat":
            jump newbie03            
        "04 Doctor chat":
            jump doctor04
        "05 Leader chat":
            jump leader05
        "06 Mechanic chat":
            jump mechanic06
        "07 Morning chat":
            jump morning07
    return
