﻿# The script of the game goes in this file.

label newbie03:
    scene campnight
    show pro logo at left
    show com stern:
        align (0.4, 1.0)
    show mec:
        align (0.6, 1.0)
    show kid:
        align (0.8, 1.0)
    show doc glare:
        align (1.0, 1.0)

    com "It's decided then. It's obvious there's only one thing to do."
    
    show doc with dissolve
    doc "Glad you see sense. Destroying it is the best option--"
    
    show com with dissolve
    com "Tie it up so we can get some sleep."
    doc "...come again?"
    mec "Makes sense. I'm out -- I've been awake for eighteen hours."

    hide mec with dissolve

    show doc glare with dissolve
    doc "Wait, how does that make any sense? It'll be far easier to sleep if that... {i}thing{/i} isn't here!"

    show com stern with dissolve
    com "It's an order. Rookie, you're up."

    kid "Yes, ma'am!"
    kid "...Umm, commander? Up next for what? The next watch?"
    com "(Sleepy mumbling)"

    hide com with dissolve

    show kid surprise with dissolve
    kid "C-Commander?"
    
    show doc with dissolve
    doc "Kid."
    
    show kid with dissolve
    kid "Can I get you anything, doc?"

    show doc glare with dissolve
    doc "Keep a close eye on that... {i}thing{/i}."

    hide doc with dissolve
    stop music fadeout 3.0
    
    kid "Sure... good night everyone?"
    kid "..."

    show kid:
        linear 1.0 align (1.0, 1.0)

    kid "So..."
    kid "Umm, hey?"

    # TODO: Use AI sprite
    pro "..."

    $ responded_03 = False
    menu:
        "How may I be of assistance?":
            play music bgmdefaultintro noloop fadeout 1.0
            queue music bgmdefaultloop loop

            pro "Designation: Kid. How may I be of assistance?"
            kid "Ouch."
            pro "Are you physically injured? My optical sensors are unable to detect any injuries."
            kid "It's not that. Everyone here calls me a kid."
            pro "Would you like to change your designation to Rookie?"
            kid "No! I mean, not that, please."
            pro "Understood, Kid."
            $ responded_03 = True
        "...":
            pro "..."
            kid "You're the quiet type, huh. Not that I know anything about your kind."
            kid "Wait, I didn't mean that in a bad way or anything! Mum says it's rude to say things like that. Like it's discrimination or something."
            kid "..."
            kid "Umm, do you, like, get offended? Even if just a little bit?"
    kid "..."
    kid "You know, I think the commander sees something in you. Like, maybe there's something special about you."
    kid "The others seem to think you're acting pretty oddly for a machine. But it's not like they've seen every machine ever, you know?"
    kid "So maybe you're actually pretty normal, and we just didn't know anything about machines."

    pro "..."
    menu:
        "I don't think that's what the commander thinks.":
            show pro neutral with dissolve
            pro "I don't think that's what the commander thinks. I believe she only wants me alive so she can interrogate me."
            if responded_03:
                kid "Really? Oh, wait a second, you sound a bit different now than you did before. Like, less robotic. Is it like a machine thing?"
                kid "Wait, I didn't mean that in a bad way or anything! Mum says it's rude to say things like that. Like it's discrimination or something."
            else:
                play music bgmdefaultintro noloop fadeout 1.0
                queue music bgmdefaultloop loop

                kid "Whoa, you can speak! That's great!"
                kid "Uh, I mean, of course you can. I didn't mean anything bad by it either. I swear I'm not a bigot."
                $ responded_03 = True
        "...":
            pro "..."
            kid "Yeah, that's it. I bet there's a lot we don't know about each other. The Machines and humans, I mean."
            kid "But I guess us two, too. Since you're stuck with me for now, we may as well get to know each other, right?"
            pro "..."

    kid "So, what is your name? I mean, you have one of those, right? If not, I'll just name you something. Like, Mary, or Faith, or something."

    menu:
        "Don't call me that.":
            show pro neutral with dissolve
            pro "Don't call me that."
        "Don't call me that, rookie.":
            show pro neutral with dissolve
            pro "Don't call me that, rookie."

    kid "Oh, sorry! I didn't mean to..."
    kid "Hang on..."
    if responded_03:
        kid "The way your face changes with the tone of your voice... you're like Gramps on Mum's side!"
    else:
        play music bgmdefaultintro noloop fadeout 1.0
        queue music bgmdefaultloop loop

        kid "Whoa, you can speak! That's great!"
        kid "And the way your face changes... you're like Gramps on Mum's side!"

    menu:
        "Excuse me?":
            pro "Excuse me? Can you elaborate on that?"
        "Are you saying I'm old?":
            pro "Are you saying... I'm old?"
            kid "Well, you kinda are... have you seen the rust on you? Anyway..."

    kid "He has this thing called dissociative identity disorder or something, you know, the one where one person has many personalities."
    kid "Do all machines have many personalities? Oh! Do you, like, shop for personalities in a store and buy them? Us humans used to do that a lot."

    menu:
        "Used to buy things in stores?":
            kid "Yeah! There were tons of things you can buy in stores, even things you can eat."
        "Used to shop for personalities?":
            kid "No, you go to theatre school for that."

    kid "I like this face the most. You're easier to talk to when you're on this face."
    pro "I see... I suppose I don't blame you."
    kid "*Yaaawwn*"
    kid "Mmm... what's the time... ah, it's the end of my shift. Guess it's time for me to wake..."
    kid "To wake..."

    "The kid suddenly snaps up."

    show kid surprise with dissolve
    kid "Wait, who's in the next shift?!"

    "He looks around as if an answer would somehow materialise behind him."

    show kid with dissolve
    kid "Aww crap, I forgot to ask the commander who's doing the next shift! Who do I wake???"
    kid "{i}Yaaawwn{/i}"
    kid "Oh I know, I can just do a double... double shift..."

    "He nods off for a moment before snapping awake."

    kid "Maybe not."

    menu:
        "It's the commander's turn.":
            pro "I believe it's the commander's turn."
            kid "Yeah? Yeah, that's right, the commander said something like that, didn't she? Thanks!"
            hide kid with dissolve
            "The kid trots away into one of the tents, and about a minute later the commander stumbles out groggily."
            jump leader05
        "Wake the doctor up.":
            pro "You probably should wake the doctor up."
            kid "Yeah? Yeah, that sounds about right. Thanks!"
            hide kid with dissolve
            "The kid trots away into one of the tents."

            doc "Why are you waking me up right now?! I'm on the next shift!"
            kid "Sorry!!!"
            doc "Whatever... I'm up now."
            jump doctor04
